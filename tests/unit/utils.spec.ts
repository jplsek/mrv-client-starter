import { expect } from 'chai'
import { generateColor } from '@/utils'

describe('utils.ts', () => {
  it('has partners with the same colors when alternating', () => {
    const partners = []
    for (let i = 0; i < 72; i++) {
      partners.push({
        id: i,
        partner: (i + 1) % 2 ? i + 1 : i - 1
      })
    }

    for (const partner of partners) {
      const c1 = generateColor(partner.id, partners[partner.partner].id)
      const c2 = generateColor(partners[partner.partner].id, partner.id)
      expect(c1).equal(c2)
    }
  })

  it('has partners with the same colors when opposite', () => {
    const partners = []
    for (let i = 0; i < 72; i++) {
      partners.push({
        id: i,
        partner: 71 - i
      })
    }

    for (const partner of partners) {
      const c1 = generateColor(partner.id, partners[partner.partner].id)
      const c2 = generateColor(partners[partner.partner].id, partner.id)
      expect(c1).equal(c2)
    }
  })

  it('has partners where every color is unique when alternating', () => {
    const partners = []
    for (let i = 0; i < 72; i++) {
      partners.push({
        id: i,
        partner: (i + 1) % 2 ? i + 1 : i - 1
      })
    }

    const colors = new Set()
    for (const partner of partners) {
      colors.add(generateColor(partner.id, partners[partner.partner].id))
      colors.add(generateColor(partners[partner.partner].id, partner.id))
    }
    expect(colors.size).equal(36)
  })

  it('has partners where every color is unique when opposite', () => {
    const partners = []
    for (let i = 0; i < 72; i++) {
      partners.push({
        id: i,
        partner: 71 - i
      })
    }

    const colors = new Set()
    for (const partner of partners) {
      colors.add(generateColor(partner.id, partners[partner.partner].id))
      colors.add(generateColor(partners[partner.partner].id, partner.id))
    }
    expect(colors.size).equal(36)
  })
})
