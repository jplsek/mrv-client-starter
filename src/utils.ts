export function generateColor (id1: number, id2: number) {
  // Make sure to run unit tests if you change this!

  // assume 144 max
  const id = id1 + id2
  // add some uniqueness, which makes the max 72
  const next = id - Math.min(id1, id2)
  // max = 360 (72 * 5)
  const max = next * 5

  return 'hsl(' + max + ', 100%, 80%)'
}
