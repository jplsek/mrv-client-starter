# mrv-client

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Environment Variables

See [Modes and Environment Variables](https://cli.vuejs.org/guide/mode-and-env.html)

### License

GPLv3 (The Frontend UI Only - Not any backend)
